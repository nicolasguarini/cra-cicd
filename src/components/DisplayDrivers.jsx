import React, { useEffect, useState } from "react";

/**
 * Componente per visualizzare i dati dei piloti di Formula 1.
 * Effettua una chiamata all'API http://ergast.com/api/f1/2022/drivers.json.
 *
 * @component
 */
const DisplayDrivers = () => {
  const [drivers, setDrivers] = useState(null);

  useEffect(() => {
    /**
     * Funzione asincrona per recuperare i dati dei piloti dall'API e impostarli nello stato.
     *
     * @async
     * @function
     * @throws {Error} Se si verifica un errore durante la chiamata all'API.
     */
    const fetchData = async () => {
        try {
          const response = await fetch(
            "http://ergast.com/api/f1/2022/drivers.json"
          );
    
          if (!response.ok) {
            throw new Error("Errore nella chiamata all'API");
          }
    
          const data = await response.json();
          setDrivers(data);
        } catch (error) {
          console.error(error);
        }
      };

      fetchData()
  }, [])
  
  
  return <div>
        <h1>F1 Drivers</h1>
        {drivers ? (
        <pre>{JSON.stringify(drivers, null, 2)}</pre>
      ) : (
        <p>Caricamento in corso...</p>
      )}
    </div>;
};

export default DisplayDrivers;
