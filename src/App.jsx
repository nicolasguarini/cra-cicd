import DisplayDrivers from './components/DisplayDrivers';

function App() {
  return (
    <div className='container'>
        <DisplayDrivers />
    </div>
  );
}

export default App;
