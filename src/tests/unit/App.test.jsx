import { render, screen } from '@testing-library/react';
import App from '../../App';

test('renders F1', () => {
  render(<App />);
  const linkElement = screen.getByText(/F1/i);
  expect(linkElement).toBeInTheDocument();
});
