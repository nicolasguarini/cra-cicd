import React from 'react';
import { render, waitFor, screen } from '@testing-library/react';
import DisplayDrivers from '../../components/DisplayDrivers';

test('renderizza il componente senza errori', () => {
  render(<DisplayDrivers />);
});

test('visualizza il messaggio di caricamento durante il caricamento dei dati', () => {
  render(<DisplayDrivers />);
  const loadingMessage = screen.getByText('Caricamento in corso...');
  expect(loadingMessage).toBeInTheDocument();
});

test('visualizza i dati dei piloti dopo il caricamento', async () => {
  render(<DisplayDrivers />);
  await waitFor(() => {
    const driversData = screen.getByText(/F1 Drivers/);
    expect(driversData).toBeInTheDocument();
  });
});
