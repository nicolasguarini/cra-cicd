test('test di staging per verifica integrazione con API', async () => {
    // Esegui una chiamata all'API di staging o di test
    const apiUrl = 'https://ergast.com/api/f1/2022/drivers.json';
    const response = await fetch(apiUrl);
  
    // Verifica che la risposta sia stata ricevuta con successo
    expect(response.status).toEqual(200);

  });
  